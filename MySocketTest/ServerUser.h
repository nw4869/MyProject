#pragma once
#include "User.h"

#include <Winsock2.h>

class ServerUser;

struct Client
{
	ServerUser *par;
	int index;
	SOCKET sock;
	SOCKADDR_IN addr;
	bool ok;
	Client() : ok(0){}
	Client(ServerUser *p, int i) : par(p), index(i){}
};

class ServerUser : public User
{
private:
	SOCKET sockSrv;
	//SOCKADDR_IN addrClient;
	HANDLE hServerThread;
	HANDLE hClientThread[MAX_CONN];
	int nconn;
	Client clt[MAX_CONN];
public:
	ServerUser();
	~ServerUser(void);
	virtual bool init(int port, char* hostIp = 0);
	virtual void endTalk();
	void endTalk(int i);
	int getNumOfConn();
	void virtual startTalk();
private:
	friend DWORD WINAPI waitConn(LPVOID lp);
	void virtual reciver();
	bool virtual sender(char *msg, int flag = 0);
	friend DWORD WINAPI wait_for_msg_per(LPVOID lp);
	int getFreeConn();
};
