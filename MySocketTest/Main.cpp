#pragma comment(lib,"ws2_32.lib")
#include <Winsock2.h>
#include <stdio.h>
#include <string.h>
#include <conio.h>
#include "Main.h"
#include "ServerUser.h"
#include "ClientUser.h"
#include "User.h"
using namespace std;

int input(char *ip, char* name)
{
	printf("Welcome!!\n");
	//char ipSrv[LENGTH] = "127.0.0.1";
	strcpy(ip, "127.0.0.1");
	char mode;
	int nmode;
	printf("Please input mode, connet(C) or wait(W)?\n");

	while(true)
	{
		scanf("%c", &mode);
		getchar();
		if(!(toupper(mode) == 'C' || toupper(mode) == 'W'))
			printf("Input error, please reinput.\n");
		else 
			break;
	}
	if(toupper(mode) == 'C')
	{
		printf("please input server IP address\n");
		gets(ip);
		nmode = 0;
	}
	else
	{
		nmode = 1;
	}

	printf("Please input your name.\n");

	gets(name);

	return nmode;
}

bool init(User *user, char *ip, char* name)
{
	user->setName(name);
	if(!user->init(PORT, ip))
		return false;
	return true;
}

void startTalk(User* user)
{
	user->startTalk();
}

void getNetIp(char *ip)
{
	//创建连向服务器的套接字
	SOCKET sock = socket(AF_INET,SOCK_STREAM,0);
	//创建地址信息
	SOCKADDR_IN hostAddr;
	hostAddr.sin_addr.S_un.S_addr = inet_addr("183.238.101.232");
	hostAddr.sin_family = AF_INET;
	hostAddr.sin_port = htons(80);
	//连接服务器
	if(connect(sock,(sockaddr*)&hostAddr,sizeof(sockaddr)) == SOCKET_ERROR)
	{
		printf("connect error!\n");
		return;
	}
	char szBuffer[1024] = {"GET /ic.asp HTTP/1.1\r\nHost:iframe.ip138.com\r\nConnection:Close\r\n\r\n"};
	//向服务器发送数据 
	if(send(sock,szBuffer,strlen (szBuffer),0) == SOCKET_ERROR)
		printf("send error\n");
	//从服务器获得数据 
	if(recv(sock,szBuffer,1024,0) == SOCKET_ERROR)
		printf("recv error\n");
	//output
	printf("Net Ip address is:\n  ");
	char *p = szBuffer;
	int flag = 0;
	while(*p != ']')
	{
		if(*(p-1) == '[') flag = 1;
		if(flag)
			printf("%c", *p);
		p++;
	}
	printf("\n");
	//printf("%s\n",szBuffer);
	closesocket(sock);
	//_getch();  
}

int main()
{
	WORD wVersionRequested;
	WSADATA wsaData;

	wVersionRequested = MAKEWORD( 1, 1 );
	WSAStartup( wVersionRequested, &wsaData );

	ServerUser server;
	ClientUser client;
	User *user;

	//getNetIp(NULL);

	//User::printHostIp();

	char ip[LENGTH], name[LENGTH];
	int err;
	do 
	{
		if(input(ip, name) == 1)
		{
			User::printHostIp();
			user = &server;
		}
		else
		{
			user = &client;
		}
		err = init(user, ip, name);
	} while (!err);
		
	startTalk(user);

	return 0;
}