#pragma once
#include "User.h"
class ClientUser : public User
{
public:
	virtual bool init(int port, char* hostIp = 0);
public:
	ClientUser();
	~ClientUser(void);
private:
	friend DWORD WINAPI waitConn(LPVOID lp);
};

