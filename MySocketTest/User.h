#pragma once
#include "Main.h"
#include <WinSock2.h>
class User
{
protected:
	char name[LENGTH];
	SOCKADDR_IN addr;
	SOCKET sock;
	HANDLE hThread;
	bool connOK;
	int port;

public:
	char* getName();
	char* getIp();
	void virtual startTalk();
	void virtual endTalk();
	bool virtual init(int port, char* ip = 0) = 0;
	void setName(char *username);
	static void printHostIp(); 
public:
	User();
	~User(void);
private:
	void initAddr(int port, char* ip = 0);
	friend DWORD WINAPI wait_for_msg(LPVOID lp);
	void virtual reciver();
};

