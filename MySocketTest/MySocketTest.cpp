//#define _CRT_SECURE_NO_WARNINGS
//#pragma comment(lib,"ws2_32.lib")
//#include <stdio.h>
//#include <string.h>
//#include <Winsock2.h>
//#include <conio.h>
//
//const int LENGTH = 1024;
//const int PORT = 6000;
//const int MAX_CONN = 5;
//
//DWORD WINAPI serverThread(LPVOID lp)
//{
//	SOCKET sockSrv = socket(AF_INET, SOCK_STREAM, 0);
//
//	SOCKADDR_IN addrSrv;
//	addrSrv.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
//	addrSrv.sin_family = AF_INET;
//	addrSrv.sin_port = htons(PORT);
//
//	bind(sockSrv, (SOCKADDR*)&addrSrv, sizeof(SOCKADDR));
//
//	listen(sockSrv, MAX_CONN);
//
//	SOCKADDR_IN addrClient;
//	int len = sizeof(SOCKADDR);
//
//	while(true)
//	{
//		printf("waiting...\n");
//		SOCKET sockConn = accept(sockSrv, (SOCKADDR*)&addrClient, &len);
//		printf("%s to here!\n", inet_ntoa(addrClient.sin_addr));
//		char sendBuf[LENGTH];
//		sprintf(sendBuf, "Welcome %s to here!", inet_ntoa(addrClient.sin_addr));
//		send(sockConn, sendBuf, strlen(sendBuf)+1, 0);
//
//		char recvBuf[LENGTH];
//		int nrecv;
//		while((nrecv = recv(sockConn, recvBuf, LENGTH, 0)) != SOCKET_ERROR)
//		{
//			recvBuf[nrecv] = 0;
//			puts(recvBuf);
//		}
//		printf("%s went out...\n", inet_ntoa(addrClient.sin_addr));
//		closesocket(sockConn);
//	}
//	return 0;
//}
//
//DWORD WINAPI wait_for_msg(LPVOID lp)
//{
//	SOCKET *sock =(SOCKET *)lp;
//	while(true)
//	{
//		int nrecv;
//		char recvBuff[LENGTH];
//		nrecv = recv(*sock, recvBuff, LENGTH, 0);
//		if(nrecv > 0)
//		{
//			recvBuff[nrecv] = 0;
//			printf("%s\n", recvBuff);
//		}
//		else if(nrecv == SOCKET_ERROR)
//		{
//			printf("Disconnected from the server!\n");
//			_getch();
//			return 1;
//		}
//	}
//	return 0;
//}
//
//void Connect(int mode)
//{
//	char ipSrv[LENGTH];
//	SOCKET sockClient = socket(AF_INET, SOCK_STREAM, 0);
//	SOCKADDR_IN addrSrv;
//
//	while(true)
//	{
//		if(mode == 2)
//		{
//			strcpy(ipSrv, "127.0.0.1");
//		}
//		else
//		{
//			printf("please input server IP address\n");
//			gets(ipSrv);
//		}
//
//		addrSrv.sin_addr.S_un.S_addr = inet_addr(ipSrv);
//		addrSrv.sin_family = AF_INET;
//		addrSrv.sin_port = htons(PORT);
//
//		printf("connecting...\n");
//		if(connect(sockClient, (SOCKADDR*)&addrSrv, sizeof(SOCKADDR)) != SOCKET_ERROR)
//			break;
//		printf("connect error!\n");
//	}
//	printf("connect success!\n");
//
//	send(sockClient, "hello everyone", sizeof("hello everyone")+1, 0);
//	
//	HANDLE hThread;
//	hThread = CreateThread(NULL, 0, wait_for_msg, (LPVOID*)&sockClient, NULL, 0);
//
//	while (true)
//	{
//		int nrecv;
//		char sendBuff[LENGTH];
//		gets(sendBuff);
//
//		nrecv = send(sockClient, sendBuff, strlen(sendBuff), 0);
//		if(nrecv != strlen(sendBuff))
//		{
//			printf("send error\n");
//		}
//	}
//	closesocket(sockClient);
//
//}
//
//int modeSelet()
//{
//	char mode;
//	printf("Please input mode, connet(C) or wait(W)?\n");
//	
//	while(true)
//	{
//		scanf("%c", &mode);
//		getchar();
//		if(mode == 'C')
//			return 0;
//		else if(mode == 'W')
//			return 1;
//		else
//			printf("Input error, please reinput.\n");
//	}
//	return -1;
//}
//
//int main()
//{
//	//printf("I am server\n");
//	WORD wVersionRequested;
//	WSADATA wsaData;
//	
//	wVersionRequested = MAKEWORD(1, 1);
//	WSAStartup(wVersionRequested, &wsaData);
//
//	int mode = modeSelet();
//	if(mode)
//	{
//		HANDLE hThread;
//		hThread = CreateThread(NULL, 0, serverThread, (LPVOID)NULL, 0, NULL);
//		Connect(2);
//	}
//	else
//	{
//		Connect(mode);
//	}
//
//	WSACleanup();
//	return 0;
//}