#include "ClientUser.h"
#include "User.h"
#include <WinSock2.h>
#include <stdio.h>


ClientUser::ClientUser() 
{
}


ClientUser::~ClientUser(void)
{
	endTalk();
}

bool ClientUser::init(int port, char* hostIp /* = 0 */)
{
	this->port = port;
	sock = socket(AF_INET, SOCK_STREAM, 0);

	addr.sin_addr.S_un.S_addr = inet_addr(hostIp);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);

	printf("connecting...\n");
	if(connect(sock, (SOCKADDR*)&addr, sizeof(SOCKADDR)) == SOCKET_ERROR)
	{
		printf("connect error!\n");
		return false;
	}
	printf("connect success!\n");
	connOK = true;
	send(sock, "hello everyone", sizeof("hello everyone")+1, 0);

	return true;
}